@echo off

set THIS_FOLDER=%~dp0

set CP="%THIS_FOLDER%/../lib/core.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/core-gui.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/core-util.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/apache-commons-io.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/apache-commons-compress.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/apache-commons-configuration.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/apache-commons-lang.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/slf4j-log4j.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/slf4j-api.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/log4j.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/synthetica.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/syntheticaSimple2D.jar"
set CP=%CP%;"%THIS_FOLDER%/../lib/jsch.jar"

set JAVA_EXE="%THIS_FOLDER%\..\..\jre\bin\java"

%JAVA_EXE% -cp %CP% eu.engys.gui.CollectInfo
