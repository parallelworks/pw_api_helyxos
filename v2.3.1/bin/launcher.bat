@echo off

set XFLAG=-Xms256m
IF NOT EXIST "%ProgramFiles(x86)%" GOTO 32BIT
set XFLAG=%XFLAG% -Xmx1024m
set XFLAG=%XFLAG% -XX:+UseSerialGC
set XFLAG=%XFLAG% -Djava.rmi.server.hostname=127.0.0.1

set JRE_NAME=jre
goto END
:32BIT
set JRE_NAME=jre
set XFLAG=%XFLAG% -Xmx512m
:END

set HOME_DIR=%~dp0..
set PATH=%HOME_DIR%\ext;%PATH%
set SPLASH_SCREEN=-splash:"%HOME_DIR%\img\splash.png"
set MAIN_CLASS=%1
set CONSOLE=""
set CLASSPATH=
set ARGS=

:START_PARAMETERS
if x==%2x goto END_PARAMETERS
if %2==-noconsole (
 set CONSOLE=%2
) else (
 set ARGS=%ARGS% %2
)
shift
goto START_PARAMETERS
:END_PARAMETERS

if %CONSOLE%==-noconsole (
    set JAVA_EXE=start "" "%HOME_DIR%\..\%JRE_NAME%\bin\javaw"
) else (
    set JAVA_EXE="%HOME_DIR%\..\%JRE_NAME%\bin\java"
)

rem set JAVA_EXE=%JAVA_EXE% -Dcom.sun.management.jmxremote.port=3333
rem set JAVA_EXE=%JAVA_EXE% -Dcom.sun.management.jmxremote.ssl=false
rem set JAVA_EXE=%JAVA_EXE% -Dcom.sun.management.jmxremote.authenticate=false


for /f "delims=" %%i in ('dir /b /s "%HOME_DIR%\lib\*.jar"') do call :concat "%%i"
goto :next

:concat
set CLASSPATH=%CLASSPATH%%1;
goto :eof

:next
set CLASSPATH=-classpath %CLASSPATH%
%JAVA_EXE% %XFLAG% %SPLASH_SCREEN% %CLASSPATH% %MAIN_CLASS% %ARGS%

echo "---------------"
echo %JAVA_EXE% %XFLAG% %CLASSPATH% %MAIN_CLASS% %ARGS%
echo "---------------"

if %CONSOLE% equ "" (
 if %ERRORLEVEL% equ 0 (
      echo SYSTEM EXIT
  ) else (
      echo FATAL ERROR: SYSTEM ABORTED!
      "%HOME_DIR%\bin\collectInfo.bat" 	  
  )
)

exit /B 0