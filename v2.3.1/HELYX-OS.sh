#!/bin/bash

THIS_FILE=`readlink -f $0`
THIS_FOLDER=`dirname $THIS_FILE`

export APPLICATION_LAUNCHER=$THIS_FILE
source $THIS_FOLDER/bin/launcher.conf

launchSuite $@