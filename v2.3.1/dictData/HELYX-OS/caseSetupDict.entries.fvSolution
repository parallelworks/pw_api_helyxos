fvSolution_SIMPLE
{
    nNonOrthogonalCorrectors 0;
    pressureImplicitPorousity false;

    pRefCell        0;
    pRefValue       0;
    residualControl
    {
    	"(U|k|epsilon|omega|nuTilda|T|p_rgh|p)" 1e-5;
    }
}
fvSolution_PISO
{
    nCorrectors     3;
    nNonOrthogonalCorrectors 0;
    pressureImplicitPorousity false;

    pRefCell        0;
    pRefValue       0;
    rhoMin          rhoMin [ 1 -3 0 0 0 0 0 ] 0.5;
    rhoMax          rhoMax [ 1 -3 0 0 0 0 0 ] 2.0;
    pMin            pMin   [1 -1 -2 0 0 0 0 ] 100;
}
fvSolution_PIMPLE
{
    momentumPredictor yes;
    nOuterCorrectors 1;
    nCorrectors     2;
    nNonOrthogonalCorrectors 0;
    pRefCell        0;
    pRefValue       0;
    rhoMin          rhoMin [ 1 -3 0 0 0 0 0] 0.5;
    rhoMax          rhoMax [ 1 -3 0 0 0 0 0] 2.0;

    residualControl
    {
        "(U|k|epsilon|omega|nuTilda|T|p_rgh|p)"
        {
            relTol          0;
            tolerance       0.0001;
        }
        "alpha.*"
        {
            relTol          0;
            tolerance       0.0001;
        }
    }
}
fvSolution_solvers_steady
{
    "(p|p_rgh|f)"
    {
        solver 					GAMG;
        agglomerator 			faceAreaPair ; 
        mergeLevels 			1 ; 
        cacheAgglomeration 		true ;
        nCellsInCoarsestLevel 	200 ; 
        tolerance 				1e-7; 
        relTol 					0.01 ; 
        smoother 				GaussSeidel ; 
        nPreSweeps 				0 ; 
        nPostSweeps 			2 ; 
        nFinestSweeps 			2 ; 
        minIter     			1 ; 
    }

    "(U|k|kl|kt|q|zeta|epsilon|R|nuTilda|omega|h|T|v2)"
    {
        solver      smoothSolver;
        smoother    GaussSeidel;
        tolerance   1e-6;
        relTol      0.1 ;
        minIter     1 ; 
    }

    rho
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }
    rhoFinal
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }  
    e
    {
        solver 			PBiCG;
        preconditioner 	DILU;
        tolerance 		1e-06;
        relTol 			0.1 ;
        minIter     	1 ; 
    }
}
fvSolution_solvers_PIMPLE
{

    "(p|p_rgh|f)"
    {
        solver 					GAMG;
        agglomerator 			faceAreaPair ; 
        mergeLevels 			1 ; 
        cacheAgglomeration 		true ;
        nCellsInCoarsestLevel 	200 ; 
        tolerance 				1e-10 ; 
        relTol 					0.01 ; 
        smoother 				GaussSeidel ; 
        nPreSweeps 				0 ; 
        nPostSweeps 			2 ; 
        nFinestSweeps 			2 ; 
        minIter     			1 ; 
    }

    "(p|p_rgh|f)Final"
    {
        solver 					GAMG;
        smoother 				GaussSeidel ; 
        agglomerator 			faceAreaPair ; 
        mergeLevels 			1 ; 
        cacheAgglomeration 		true ;
        nCellsInCoarsestLevel 	200 ; 
        tolerance 				1e-6 ; 
        relTol 					0 ; 
        nPreSweeps 				0 ; 
        nPostSweeps 			2 ; 
        nFinestSweeps 			2 ; 
        minIter     			1 ; 
    }

    "(U|k|kl|kt|q|zeta|epsilon|R|nuTilda|omega|h|T|v2)"
    {
        solver      smoothSolver;
        smoother    GaussSeidel;
        tolerance   1e-6;
        relTol      0.1 ;
        minIter     1 ; 
    }
    "(U|k|kl|kt|q|zeta|epsilon|R|nuTilda|omega|h|T|v2)Final"
    {
        solver      smoothSolver;
        smoother    GaussSeidel;
        tolerance   1e-6;
        relTol      0;
        minIter     1 ; 
    }
    rho
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }
    rhoFinal
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }
    e
    {
        solver 			PBiCG;
        preconditioner 	DILU;
        tolerance 		1e-06;
        relTol 			0.1 ;
        minIter     	1 ; 
    }
}
fvSolution_solvers_PISO
{

    "(p|p_rgh|f)"
    {
        solver 					GAMG;
        agglomerator 			faceAreaPair ; 
        mergeLevels 			1 ; 
        cacheAgglomeration 		true ;
        nCellsInCoarsestLevel 	200 ; 
        tolerance 				1e-10 ; 
        relTol 					0.01 ; 
        smoother 				GaussSeidel ; 
        nPreSweeps 				0 ; 
        nPostSweeps 			2 ; 
        nFinestSweeps 			2 ; 
        minIter     			1 ; 
    }

    "(p|p_rgh|f)Final"
    {
        solver 					GAMG;
        smoother 				GaussSeidel ; 
        agglomerator 			faceAreaPair ; 
        mergeLevels 			1 ; 
        cacheAgglomeration 		true ;
        nCellsInCoarsestLevel 	200 ; 
        tolerance 				1e-6 ; 
        relTol 					0.0001 ; 
        nPreSweeps 				0 ; 
        nPostSweeps 			2 ; 
        nFinestSweeps 			2 ; 
        minIter     			1 ; 
    }

    "(U|k|kl|kt|q|zeta|epsilon|R|nuTilda|omega|h|T|v2)"
    {
        solver      smoothSolver;
        smoother    GaussSeidel;
        tolerance   1e-6;
        relTol      0;
        minIter     1 ; 
    }
    rho
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }
    rhoFinal
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       0;
        relTol          0;
        minIter     	1 ; 
    }
    e
    {
        solver 			PBiCG;
        preconditioner 	DILU;
        tolerance 		1e-06;
        relTol 			0;
        minIter     	1 ; 
    }
}
fvSolution_relaxationFactors_steady
{
    fields
    {
        p_rgh           0.3;
        p               0.3;
        rho             0.05;
    }
    equations
    {
        U               0.7;
        h               0.5;
        "(k|kl|kt|q|zeta|epsilon|R|nuTilda|omega|T|v2|f)" 0.7;
    }
}
fvSolution_relaxationFactors_trans
{
    fields
    {
        p 			1;
        p_rgh		1;
        rho         1;
    }
    equations
    {
        epsilon   	1;
        f          	1;
        h   		1;
        k   		1;
        kl          1;
        kt          1;
        nuTilda   	1;
        omega   	1;
        q          	1;
        R 			1;
        T   		1;
        U   		1;
        v2          1;
        zeta    	1;
        "alpha.*"   1;
    
        epsilonFinal   	1;
        fFinal         	1;
        hFinal   		1;
        kFinal   		1;
        klFinal   		1;
        ktFinal         1;
        nuTildaFinal   	1;
        omegaFinal   	1;
        qFinal        	1;
        RFinal 			1;
        TFinal   		1;
        UFinal   		1;
        v2Final         1;
        zetaFinal    	1;
        "alpha.*Final" 	1;
    }

}
