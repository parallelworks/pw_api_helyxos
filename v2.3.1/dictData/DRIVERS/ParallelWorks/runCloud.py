#!/usr/bin/env python

import time, os, sys
from client import Client

# inputs
api_key = os.environ['APIKEY']
workspace_name = os.environ['WORKSPACE']
helyxos_command = os.environ['COMMAND']
pw_url = "https://eval.parallel.works"
dataset_name = "inputs.tgz"
workflow_name = "helyxos_runner_rsync_v2"

# create a new Parallel Works client
c = Client(pw_url,api_key)

# get workspace id from workspace name
wid = c.get_wid(workspace_name)

# upload the dataset
print "Uploading the Case"
sys.stdout.flush()
did = c.upload_dataset(wid,dataset_name)

# poll file upload state until complete
while True:
    time.sleep(1)
    state = c.get_dataset_state(did)
    print state.upper()
    sys.stdout.flush()
    if state == 'ok':
        break

# start the pw job
print "Executing the Case"
sys.stdout.flush()
jid = c.start_job(wid,did,workflow_name,helyxos_command)

# write the jid to a file for later stop/kill
f = open('cloud.job','w')
f.write(jid)
f.close()

lastline=0
laststatus=""
while True:
    time.sleep(1)
    try:
        state,status = c.get_job_state(jid)
    except:
        state="starting"
        status=""
    if laststatus!=status:
        print status
        sys.stdout.flush()
    laststatus=status
    tail = c.get_job_tail(jid,lastline)
    if (tail != ""):
        print tail
        # append the tail to the log file
        f = open(os.environ['LOG'], 'a')
        f.write(tail)
        f.close()
        lastline+=len(tail.split('\n'))
        sys.stdout.flush()
    if state == 'ok':
        rid = c.get_result_id(jid,"results")
        break
    elif (state == 'deleted' or state == 'error'):
        raise Exception('Simulation had an error. Please try again')

# get the result dataset url
durl = c.get_download_url(rid)
sys.stdout.flush()

f = open('cloud.out','w')
f.write(durl)
f.close()
