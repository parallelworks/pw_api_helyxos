#!/bin/bash

export COMMAND=$1

echo "Running $COMMAND on Parallel Works Cloud"

# check if python 
if ! which python >/dev/null 2>&1; then
    echo "Parallel Works needs Python-2.7 to run Helyx-OS on the cloud."
    echo "Please install Python-2.7 using the command below:"
    echo "sudo apt-get install python"
    exit 1
fi

# check if the requests module is installed
if ! python -c "import requests" >/dev/null 2>&1; then
    echo "Parallel Works needs the Python 'requests' module to run Helyx-OS on the cloud."
    echo "Please install the Python 'requests' module using the command below:"
    echo "pip install requests"
    exit 1
fi

# export the cloud variables so they can be found by python
source cloud.env;export APIKEY=$APIKEY;export WORKSPACE=$WORKSPACE;export PULLRESULTS=$PULLRESULTS

echo "Compressing Input Files"
rm *.tgz *.tar.gz >/dev/null 2>&1
# tar up the input files and exclude the unnecessary files based on command type
if [[ $COMMAND == *"mesh"* ]]; then
	tar -czf inputs.tgz * --exclude=constant/polyMesh/* --exclude=processor* >/dev/null 2>&1
else
	# exclude all the timestep directories except 0 (because these will be regenerated if run is successful)
	tar -czf inputs.tgz * --exclude=00* --exclude=1* --exclude=2* --exclude=3* --exclude=4* --exclude=5* --exclude=6* --exclude=7* --exclude=8* --exclude=9* >/dev/null 2>&1
fi

./runCloud.py
ec=$?

if [[ "$ec" == "0" ]];then
	echo "Parallel Works Run Successful"
	if [[ $COMMAND == *"mesh"* ]];then
		rm constant/polyMesh/* processor* 00* 1* 2* 3* 4* 5* 6* 7* 8* 9* -R >/dev/null 2>&1
		echo "Pulling Results on Completion:"
	else
		rm 00* 1* 2* 3* 4* 5* 6* 7* 8* 9* controlDict -R >/dev/null 2>&1
		if [[ "$PULLRESULTS" == "false" ]];then
			echo "Not Pulling Results on Completion:"
		else
			echo "Pulling Results on Completion:"
		fi
	fi
	cat cloud.out
	echo ""
else
	echo "Parallel Works Run Failed"
	rm cloud.out >/dev/null 2>&1 # remove this file in case of failure
fi

# clean up files
./cleanCase.sh

echo "Exit Code is "$ec
exit $ec
