#!/usr/bin/env python

import os, sys
from client import Client
from subprocess import call

# inputs
api_key = os.environ['APIKEY']
job_id = os.environ['JOBID']
pw_url = "https://eval.parallel.works"

c = Client(pw_url,api_key)

response = c.cancel_job(job_id)
print response

# clean up the cloud files
call(['bash',"cleanCase.sh"])