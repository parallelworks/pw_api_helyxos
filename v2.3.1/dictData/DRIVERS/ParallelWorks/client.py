import requests
import json

class Client():

    def __init__(self, url, key):
        self.url = url
        self.api = url+'/api'
        self.key = key
        self.session = requests.Session()
        self.headers = {
            'Content-Type': 'application/json'
        }
    
    def get_wid(self, name):
        req = self.session.get(self.api + "/histories?key=" + self.key)
        req.raise_for_status()
        data = json.loads(req.text)
        for w in data:
            if (w['name']==name):
                wid = w['id']
        return wid

    def upload_dataset(self, wid, filename):
        req = self.session.post(self.api + "/tools",
                                data={'key':self.key,'tool_id':'upload1','workspace_id':wid},
                                files={'files_0|file_data':open(filename, 'rb')})
        req.raise_for_status()
        data = json.loads(req.text)
        did=data['outputs'][0]['id']
        return did
    
    def get_dataset_state(self, did):
        req = self.session.get(self.api + "/datasets/"+did+"?key=" + self.key)
        req.raise_for_status()
        data = json.loads(req.text)
        return data['state']

    def get_workflow_name(self, workflow):
        req = self.session.get(self.url + "/workflow_name/"+workflow+"?key=" + self.key)
        req.raise_for_status()
        return req.text
    
    def start_job(self,wid,did,workflow,helyxos_command):
        inputs = json.dumps({"case":{"values":[{"src":"hda","id":did}]},"command": helyxos_command})
        req = self.session.post(self.api + "/tools",
                                data={'key':self.key,'tool_id':self.get_workflow_name(workflow),'workspace_id':wid,'inputs':inputs})
        req.raise_for_status()
        data = json.loads(req.text)
        jid=data['jobs'][0]['id']
        return jid

    def get_job_state(self, jid):
        req = self.session.get(self.api + "/jobs/"+jid+"/state?key=" + self.key)
        req.raise_for_status()
        data = json.loads(req.text)
        return data['state'],data['status']

    def get_job_tail(self, jid, lastline):
        req = self.session.get(self.api + "/jobs/"+jid+"/tail?key=" + self.key + "&file=_stdout_1.txt&line="+str(lastline))
        req.raise_for_status()
        data = req.text
        return data

    def get_result_id(self, jid, name):
        req = self.session.get(self.api + "/jobs/"+jid+"?key=" + self.key)
        req.raise_for_status()
        data = json.loads(req.text)
        return data['outputs'][name]['id']

    def get_download_url(self, did):
        req = self.session.get(self.api + "/datasets/"+did+"?key=" + self.key)
        req.raise_for_status()
        data = json.loads(req.text)
        return self.url+data['download_url']

    def upload_file_to_job(self,jid,filename):
        file = {'file': open(filename,'rb')}
        req = self.session.post(self.api + "/jobs/"+jid+"/upload?key="+self.key,files=file)
        req.raise_for_status()
        return req.text

    def cancel_job(self, jid):
        req = self.session.get(self.api + "/jobs/"+jid+"/cancel?key=" + self.key)
        req.raise_for_status()
        return req.text
