#!/usr/bin/env python

import os, sys
from client import Client
from subprocess import call

# inputs
api_key = os.environ['APIKEY']
job_id = os.environ['JOBID']
pw_url = "https://eval.parallel.works"

c = Client(pw_url,api_key)

response = c.upload_file_to_job(job_id,"system/controlDict")
print response
