/*--------------------------------*- Java -*---------------------------------*\
 |		 o                                                                   |                                                                                     
 |    o     o       | HelyxOS: The Open Source GUI for OpenFOAM              |
 |   o   O   o      | Copyright (C) 2012-2016 ENGYS                          |
 |    o     o       | http://www.engys.com                                   |
 |       o          |                                                        |
 |---------------------------------------------------------------------------|
 |	 License                                                                 |
 |   This file is part of HelyxOS.                                           |
 |                                                                           |
 |   HelyxOS is free software; you can redistribute it and/or modify it      |
 |   under the terms of the GNU General Public License as published by the   |
 |   Free Software Foundation; either version 2 of the License, or (at your  |
 |   option) any later version.                                              |
 |                                                                           |
 |   HelyxOS is distributed in the hope that it will be useful, but WITHOUT  |
 |   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   |
 |   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   |
 |   for more details.                                                       |
 |                                                                           |
 |   You should have received a copy of the GNU General Public License       |
 |   along with HelyxOS; if not, write to the Free Software Foundation,      |
 |   Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA            |
\*---------------------------------------------------------------------------*/

package eu.engys.core.controller.actions;

import static eu.engys.core.project.system.ControlDict.CONTROL_DICT;
import static eu.engys.core.project.system.ControlDict.END_TIME_KEY;
import static eu.engys.core.project.system.ControlDict.STOP_AT_KEY;
import static eu.engys.core.project.system.ControlDict.WRITE_NOW_KEY;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.engys.core.controller.HelyxOSController;
import eu.engys.core.controller.Command;
import eu.engys.core.controller.Controller;
import eu.engys.core.controller.ScriptFactory;
import eu.engys.core.dictionary.DictionaryUtils;
import eu.engys.core.executor.ExecutorHook;
import eu.engys.core.executor.ExecutorMonitor;
import eu.engys.core.project.Model;
import eu.engys.core.project.SolverState;
import eu.engys.core.project.openFOAMProject;
import eu.engys.core.project.state.ServerState;
import eu.engys.core.project.system.ControlDict;
import eu.engys.util.PrefUtil;

import java.util.List;
import org.apache.commons.io.FileUtils;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;

public class RunCase extends AbstractRunCommand {

    private static final Logger logger = LoggerFactory.getLogger(RunCase.class);

    public static final String ACTION_NAME = "Run Case";
    public static final String RUNNING_LABEL = "Running: ";

    protected final ScriptFactory scriptFactory;

    public RunCase(Model model, Controller controller, ScriptFactory scriptFactory) {
        super(model, controller);
        this.scriptFactory = scriptFactory;
    }

    @Override
    public void beforeExecute() {
        setupLogFolder();
        setupPostProcFolder();
        clearPolyMesh();
        setStopAtVariableToEndTime();
    }

    private void setupLogFolder() {
        File log = new File(model.getProject().getBaseDir(), openFOAMProject.LOG);
        if (!log.exists())
            log.mkdir();
    }

    private void setupPostProcFolder() {
        File log = new File(model.getProject().getBaseDir(), openFOAMProject.POST_PROC);
        if (!log.exists())
            log.mkdir();
    }

    private void clearPolyMesh() {
        model.getProject().getZeroFolder().removeNonZeroTimeFolders_GreaterThanActualTimeStep();
    }

    @Override
    public void stop() throws TimeoutException {
        int stop_refresh_time = PrefUtil.getInt(PrefUtil.SERVER_CONNECTION_REFRESH_TIME, 1000);
        int stop_max_tries = PrefUtil.getInt(PrefUtil.SERVER_CONNECTION_MAX_TRIES, 60);

        int tryIndex = 0;
        while (this.executor.getState().isDoingSomething() && (tryIndex < stop_max_tries)) {
            try {
                Thread.sleep(stop_refresh_time);
            } catch (Exception e) {
            }
            setStopAtVariableToWriteNow();
            tryIndex++;
        }
        if (tryIndex >= stop_max_tries) {
            throw new TimeoutException("Timeout stopping solver");
        }
        setStopAtVariableToEndTime();
    }
    
    @Override
    public void kill() {
        killCloudJob();
        this.executor.getService().shutdownNow();
    }
    
    private List<String> cloudEnv;
    private List<String> jobIdContent;
    private Boolean hasCloudEnv;
    private Boolean hasJobId;

    public void killCloudJob(){
        System.out.print("KILLING JOB\n");
            // this can/should be made a reusable function
            // much of this ugliness is to get the cloud variables
            File basedir = model.getProject().getBaseDir();
            File file = new File(basedir,"cloud.env");
            try {
                cloudEnv = FileUtils.readLines(file);
                hasCloudEnv = true;
            } catch (IOException e) {
                System.out.print("cloud.env file does not exist\n");
                hasCloudEnv = false;
            }
            if (hasCloudEnv == true){
                String selectedDriver = cloudEnv.get(0).split("=\"")[1].replace("\"","");
                if (selectedDriver != "Localhost"){
                    String APIKEY = cloudEnv.get(3).split("=\"")[1].replace("\"","");
                    File file1 = new File(basedir,"cloud.job");
                    try {
                        jobIdContent = FileUtils.readLines(file1);
                        hasJobId = true;
                    } catch (IOException e) {
                        System.out.print("cloud.job file does not exist\n");
                        hasJobId = false;
                    }
                    if (hasJobId == true){
                        // actually kill the job now by running the python script
                        String[] envp = { "APIKEY="+APIKEY, "JOBID="+jobIdContent.get(0) };
                        Runtime rt = Runtime.getRuntime();
                        try {
                            System.out.print("Stopping "+jobIdContent.get(0)+" cloud job in "+basedir+"\n");
                            Process pr = rt.exec("python killCloud.py", envp, basedir);
                            BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()) );
                            String line = null;
                            while ((line = in.readLine()) != null) {
                                System.out.println(line);
                            }
                           in.close();
                        }catch (Exception ex1) {
                            ex1.printStackTrace();
                        }
                    }
                }
            }
    }
    
    public void stopCloudJob(){
        System.out.print("STOPPING JOB\n");
            // this can/should be made a reusable function
            // much of this ugliness is to get the cloud variables
            File basedir = model.getProject().getBaseDir();
            File file = new File(basedir,"cloud.env");
            try {
                cloudEnv = FileUtils.readLines(file);
                hasCloudEnv = true;
            } catch (IOException e) {
                System.out.print("cloud.env file does not exist\n");
                hasCloudEnv = false;
            }
            if (hasCloudEnv == true){
                String selectedDriver = cloudEnv.get(0).split("=\"")[1].replace("\"","");
                if (selectedDriver != "Localhost"){
                    String APIKEY = cloudEnv.get(3).split("=\"")[1].replace("\"","");
                    File file1 = new File(basedir,"cloud.job");
                    try {
                        jobIdContent = FileUtils.readLines(file1);
                        hasJobId = true;
                    } catch (IOException e) {
                        System.out.print("cloud.job file does not exist\n");
                        hasJobId = false;
                    }
                    if (hasJobId == true){
                        // actually stop the job now by running the python script
                        String[] envp = { "APIKEY="+APIKEY, "JOBID="+jobIdContent.get(0) };
                        Runtime rt = Runtime.getRuntime();
                        try {
                            System.out.print("Stopping "+jobIdContent.get(0)+" cloud job in "+basedir+"\n");
                            Process pr = rt.exec("python stopCloud.py", envp, basedir);
                            BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()) );
                            String line = null;
                            while ((line = in.readLine()) != null) {
                                System.out.println(line);
                            }
                           in.close();
                        }catch (Exception ex1) {
                            ex1.printStackTrace();
                        }
                    }
                }
            }
    }
    
    private Boolean stopCloudJobFlag=false;
    
    private void setStopAtVariableToWriteNow() {
        File systemFolder = model.getProject().getSystemFolder().getFileManager().getFile();
        ControlDict controlDict = new ControlDict(new File(systemFolder, CONTROL_DICT));
        controlDict.add(STOP_AT_KEY, WRITE_NOW_KEY);
        controlDict.functionObjectsToList();
        DictionaryUtils.writeDictionary(systemFolder, controlDict, null);
        
        if (stopCloudJobFlag==false){
            // upload and send the new controlDict file if cloud job is running
            stopCloudJobFlag=true;
            stopCloudJob();
        }
        
    }

    private void setStopAtVariableToEndTime() {
        File systemFolder = model.getProject().getSystemFolder().getFileManager().getFile();
        ControlDict controlDict = new ControlDict(new File(systemFolder, CONTROL_DICT));
        if (controlDict.found(STOP_AT_KEY) && controlDict.lookup(STOP_AT_KEY).equals(WRITE_NOW_KEY)) {
            controlDict.add(STOP_AT_KEY, END_TIME_KEY);
            controlDict.functionObjectsToList();
            DictionaryUtils.writeDictionary(systemFolder, controlDict, null);
        }
    }

    protected class StartHook implements ExecutorHook {
        @Override
        public void run(ExecutorMonitor m) {
            model.getSolverModel().writeState(new ServerState(Command.RUN_CASE, SolverState.RUNNING), model);
        }
    }

}
