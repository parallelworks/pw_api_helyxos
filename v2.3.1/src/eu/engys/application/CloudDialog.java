/*--------------------------------*- Java -*---------------------------------*\
 |		 o                                                                   |                                                                                     
 |    o     o       | HelyxOS: The Open Source GUI for OpenFOAM              |
 |   o   O   o      | Copyright (C) 2012-2016 ENGYS                          |
 |    o     o       | http://www.engys.com                                   |
 |       o          |                                                        |
 |---------------------------------------------------------------------------|
 |	 License                                                                 |
 |   This file is part of HelyxOS.                                           |
 |                                                                           |
 |   HelyxOS is free software; you can redistribute it and/or modify it      |
 |   under the terms of the GNU General Public License as published by the   |
 |   Free Software Foundation; either version 2 of the License, or (at your  |
 |   option) any later version.                                              |
 |                                                                           |
 |   HelyxOS is distributed in the hope that it will be useful, but WITHOUT  |
 |   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   |
 |   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   |
 |   for more details.                                                       |
 |                                                                           |
 |   You should have received a copy of the GNU General Public License       |
 |   along with HelyxOS; if not, write to the Free Software Foundation,      |
 |   Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA            |
\*---------------------------------------------------------------------------*/


package eu.engys.application;

import eu.engys.core.project.Model;
import eu.engys.core.controller.ScriptBuilder;

import static eu.engys.util.ui.ComponentsFactory.checkField;
import static eu.engys.util.ui.ComponentsFactory.intField;
import static eu.engys.util.ui.ComponentsFactory.stringField;
import static eu.engys.util.ui.ComponentsFactory.selectField;

import java.io.File;
import java.util.List;
import java.net.URI;
import java.net.URISyntaxException;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.Dialog.ModalityType;
import java.awt.Component;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Icon;

import net.java.dev.designgridlayout.Componentizer;

import eu.engys.util.Symbols;

import eu.engys.core.OpenFOAMEnvironment;
import eu.engys.core.executor.FileManagerSupport;
import eu.engys.core.project.defaults.DictDataFolder;
import eu.engys.util.ApplicationInfo;
import eu.engys.util.PrefUtil;
import eu.engys.util.Util;
import eu.engys.util.filechooser.util.SelectionMode;
import eu.engys.util.ui.ComponentsFactory;
import eu.engys.util.ui.FileFieldPanel;
import eu.engys.util.ui.UiUtil;
import eu.engys.util.ui.ViewAction;
import eu.engys.util.ui.builder.PanelBuilder;
import eu.engys.util.ui.textfields.DoubleField;
import eu.engys.util.ui.textfields.IntegerField;
import eu.engys.util.ui.textfields.StringField;
import eu.engys.util.IOUtils;

import eu.engys.util.ui.builder.PanelBuilder;

import eu.engys.util.ArchiveUtils;
import org.apache.commons.io.FileUtils;
import java.net.MalformedURLException;
import java.io.IOException;
import java.net.URL;

public class CloudDialog {

    private JDialog dialog;

    private FileFieldPanel driverDirectory;
    private StringField driverExecutable;
    private StringField driverAPIKEY;
    private StringField driverWorkspace;
    private JCheckBox driverPull;
    private JComboBox cloudDriver;
    private JButton cloudButton;
    private JButton downloadButton;

    private JLabel errorLabel;
    private JButton okButton;
    private JButton openDefaults;
    private DictDataFolder dictDataFolder;
    
    private Boolean hasCloudEnv;
    private List<String> cloudEnv;
    private URL downloadUrl;
    
    protected Model model;

    public CloudDialog(Model model) {
        this.model = model;
        loadSettings();
        createDialog();
    }

    private void createDialog() {
        
        JScrollPane scrollPane = new JScrollPane(createCenterPanel());
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.getVerticalScrollBar().setUnitIncrement(20);

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        mainPanel.add(createButtonsPanel(), BorderLayout.SOUTH);

        dialog = new JDialog(UiUtil.getActiveWindow(), "Cloud Connection Settings", ModalityType.APPLICATION_MODAL);
        dialog.getContentPane().setLayout(new BorderLayout());
        dialog.getContentPane().add(mainPanel);
        dialog.setSize(555, 325);
        dialog.setLocationRelativeTo(null);
        dialog.setName("CloudDialog");
        dialog.getRootPane().setDefaultButton(okButton);
        dialog.setVisible(true);

    }
    
    public void loadSettings() {
        File file = new File(model.getProject().getBaseDir(),"cloud.env");
        try {
            cloudEnv = FileUtils.readLines(file);
            hasCloudEnv = true;
        } catch (IOException e) {
            System.out.print("cloud.env file does not exist\n");
            hasCloudEnv = false;
        }
    }

    private Component createCenterPanel() {

        JPanel cloudPanel = createCloudInitPanel();
        JPanel batchPanel = createBatchPanel();

        PanelBuilder builder = new PanelBuilder();
        builder.addComponent(cloudPanel);
        builder.addComponent(batchPanel);
        
        JPanel container = new JPanel(new BorderLayout());
        container.setOpaque(false);
        container.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        container.add(builder.getPanel(), BorderLayout.CENTER);
        container.add(errorLabel = new JLabel(""), BorderLayout.SOUTH);
        errorLabel.setForeground(Color.RED.darker());
        errorLabel.setName("error.label");
        return container;
        
    }
  
    private void addResettableComponent(PanelBuilder builder, String label, JComponent compToAdd, String prefKey) {
        //JButton resetButton = createResetButton(compToAdd, prefKey);
        //resetButton.setName(label + ".reset");
        compToAdd.setName(label);
        builder.addComponent(label, Componentizer.create().minAndMore(compToAdd).component());
    }
    
    private JPanel createCloudInitPanel() {
        PanelBuilder cloudBuilder = new PanelBuilder();
        //addResettableComponent(cloudBuilder, "Enable Cloud Connection", defaultHostFile = checkField(), "");
        String[] drivers = { "Localhost", "Parallel Works" };
        addResettableComponent(cloudBuilder, "Execution Driver", cloudDriver = selectField(drivers), "");
        
        cloudDriver.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedDriver = cloudDriver.getSelectedItem().toString();
                if (selectedDriver.equals("Localhost")){
                    driverDirectory.setFile(new File(""));
                    driverExecutable.setStringValue("Enter executable here.");
                    driverAPIKEY.setStringValue("Enter API key here.");
                    driverWorkspace.setStringValue("Enter cloud workspace here.");
                    driverPull.setSelected(Boolean.valueOf("false"));
                    driverDirectory.setEnabled(false);
                    driverExecutable.setEnabled(false);
                    driverAPIKEY.setEnabled(false);
                    driverWorkspace.setEnabled(false);
                    driverPull.setEnabled(false);
                    cloudButton.setVisible(false);
                    
                }else{
                    
                    if (hasCloudEnv==true){
                        // set the correct values here based on cloudEnv params
                        driverDirectory.setFile(new File(cloudEnv.get(1).split("=\"")[1].replace("\"","")));
                        driverExecutable.setStringValue(cloudEnv.get(2).split("=\"")[1].replace("\"",""));
                        driverAPIKEY.setStringValue(cloudEnv.get(3).split("=\"")[1].replace("\"",""));
                        driverWorkspace.setStringValue(cloudEnv.get(4).split("=\"")[1].replace("\"",""));
                        driverPull.setSelected(Boolean.valueOf(cloudEnv.get(5).split("=\"")[1].replace("\"","")));
                        // enable fields
                        driverDirectory.setEnabled(true);
                        driverExecutable.setEnabled(true);
                        driverAPIKEY.setEnabled(true);
                        driverWorkspace.setEnabled(true);
                        driverPull.setEnabled(true);
                        cloudButton.setVisible(true);
                        
                    }else { 
                        // set default values
                        driverDirectory.setFile(new File(ApplicationInfo.getRootPath()+"/dictData/DRIVERS/ParallelWorks"));
                        driverExecutable.setStringValue("runCloud.sh");
                        driverAPIKEY.setStringValue("");
                        driverWorkspace.setStringValue("helyxos_workspace");
                        driverPull.setSelected(Boolean.valueOf("false"));
                        // disable fields
                        driverDirectory.setEnabled(true);
                        driverExecutable.setEnabled(true);
                        driverAPIKEY.setEnabled(true);
                        driverWorkspace.setEnabled(true);
                        driverPull.setEnabled(true);
                        cloudButton.setVisible(true);
                        
                    }
                }
            }
        });
        
        return cloudBuilder.getPanel();
    }
    
    class OpenUrlAction implements ActionListener {
      @Override public void actionPerformed(ActionEvent e) {
        try {
                    Desktop.getDesktop().browse(new URI("https://eval.parallel.works/signup?starter=helyxos"));
                } catch (URISyntaxException | IOException ex) {
                    //It looks like there's a problem
                }
      }
    }
    
    private JPanel createBatchPanel() {
        
        PanelBuilder batchBuilder = new PanelBuilder();
        
        cloudButton = new JButton();
        cloudButton.setText("<HTML><FONT color=\"#000099\"><U>Click here to get a Parallel Works API key.</U></FONT></HTML>");
        cloudButton.setBorderPainted(false);
        cloudButton.setOpaque(false);
        cloudButton.setBackground(Color.WHITE);
        cloudButton.addActionListener(new OpenUrlAction());
        
        addResettableComponent(batchBuilder, "Driver Location", driverDirectory = ComponentsFactory.fileField(SelectionMode.DIRS_ONLY, "Select a folder where to save the case", false), "");
        addResettableComponent(batchBuilder, "Executable", driverExecutable = stringField(""), "");
        addResettableComponent(batchBuilder, "API Key", driverAPIKEY = stringField(""), "");
        addResettableComponent(batchBuilder, "Run Workspace", driverWorkspace = stringField(""), "");
        
        addResettableComponent(batchBuilder, "Pull Results from Cloud", driverPull = checkField(), "");

        batchBuilder.addComponent(cloudButton);
        batchBuilder.getPanel().setBorder(BorderFactory.createTitledBorder("Cloud Settings"));
    
        if (hasCloudEnv==false){
            driverDirectory.setFile(new File(""));
            driverExecutable.setStringValue("Enter executable here.");
            driverAPIKEY.setStringValue("Enter API key here.");
            driverWorkspace.setStringValue("Enter cloud workspace here.");
            driverPull.setSelected(Boolean.valueOf("false"));
            driverDirectory.setEnabled(false);
            driverExecutable.setEnabled(false);
            driverAPIKEY.setEnabled(false);
            driverWorkspace.setEnabled(false);
            driverPull.setEnabled(false);
            cloudButton.setVisible(false);
            
        }else{
            
            System.out.print(cloudEnv+"\n");

            // set the correct driver value

            String selectedDriver = cloudEnv.get(0).split("=\"")[1].replace("\"","");
            cloudDriver.setSelectedItem(selectedDriver);
            
            if (selectedDriver.equals("Localhost")){
                // set default values
                driverDirectory.setFile(new File(""));
                driverExecutable.setStringValue("Enter executable here.");
                driverAPIKEY.setStringValue("Enter API key here.");
                driverWorkspace.setStringValue("Enter cloud workspace here.");
                driverPull.setSelected(Boolean.valueOf("false"));
                // disable fields
                driverDirectory.setEnabled(false);
                driverExecutable.setEnabled(false);
                driverAPIKEY.setEnabled(false);
                driverWorkspace.setEnabled(false);
                driverPull.setEnabled(false);
                cloudButton.setVisible(false);
                
            }else{
                // set the correct values here based on cloudEnv params
                driverDirectory.setFile(new File(cloudEnv.get(1).split("=\"")[1].replace("\"","")));
                driverExecutable.setStringValue(cloudEnv.get(2).split("=\"")[1].replace("\"",""));
                driverAPIKEY.setStringValue(cloudEnv.get(3).split("=\"")[1].replace("\"",""));
                driverWorkspace.setStringValue(cloudEnv.get(4).split("=\"")[1].replace("\"",""));
                driverPull.setSelected(Boolean.valueOf(cloudEnv.get(5).split("=\"")[1].replace("\"","")));
                // enable fields
                driverDirectory.setEnabled(true);
                driverExecutable.setEnabled(true);
                driverAPIKEY.setEnabled(true);
                driverWorkspace.setEnabled(true);
                driverPull.setEnabled(true);
                cloudButton.setVisible(true);
                
            }
        }
        
        return batchBuilder.getPanel();
    }
    
    
    class DownloadResultAction implements ActionListener {
     @Override public void actionPerformed(ActionEvent e) {
                File destination = new File(model.getProject().getBaseDir(),"cloud.tar.gz");
                try {
                    org.apache.commons.io.FileUtils.copyURLToFile(downloadUrl,destination);
                } catch (MalformedURLException e1) {
                   e1.printStackTrace();
                } catch (IOException e1) {
                   e1.printStackTrace();
                }
                // untar the result file
                ArchiveUtils.unarchive(destination,model.getProject().getBaseDir());
                // delete the tar file
                try{
            		if(destination.delete()){
            			System.out.println("Cloud Download Complete");
            		}else{
            			System.out.println("Delete operation is failed.");
            		}
            	}catch(Exception e2){
            		e2.printStackTrace();
            	}
      }
    }
    
    private JPanel createButtonsPanel() {
        JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        okButton = new JButton(new AbstractAction("OK") {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
                dialog.setVisible(false);
            }
        });
        okButton.setName("OK");
        JButton cancelButton = new JButton(new AbstractAction("Cancel") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        });
        cancelButton.setName("Cancel");

        downloadButton = new JButton();
        downloadButton.setText("Download Last Cloud Result");
        downloadButton.addActionListener(new DownloadResultAction());
        downloadButton.setVisible(false);
        buttonsPanel.add(downloadButton);
        
        String selectedDriver = cloudDriver.getSelectedItem().toString();
        if (selectedDriver != "Localhost"){
            // check if cloud.out exists
            File file = new File(model.getProject().getBaseDir(),"cloud.out");
            List<String> downloadUrlList;
            Boolean hasCloudOutFile = false;
            try {
                downloadUrlList = FileUtils.readLines(file);
                downloadUrl = new URL(downloadUrlList.get(0));
                hasCloudOutFile = true;
            } catch (IOException e1) {
                System.out.print("cloud.out file does not exist\n");
                hasCloudOutFile = false;
            }
            // if exists download the result file
            if (hasCloudOutFile == true){
                downloadButton.setVisible(true);
            }
        }

        buttonsPanel.add(okButton);
        buttonsPanel.add(cancelButton);
        
        return buttonsPanel;
    }
    
    public void save() {
        // if executor does not equal localhost, save the cloud variables to a cloud.env file in the case directory
        
        String selectedDriver = cloudDriver.getSelectedItem().toString();
        
        if (selectedDriver != "Localhost"){
            
            System.out.print("saving "+selectedDriver+" variables to cloud.env\n"); 
            
            // get the cloud variables
            String directory = driverDirectory.getFile().toString();
            String executable = driverExecutable.getStringValue();
            String apikey = driverAPIKEY.getStringValue();
            String workspace = driverWorkspace.getStringValue();
            Boolean pullresults = driverPull.isSelected();
            //System.out.print(directory+" "+apikey+" "+workspace+" "+pullresults+"\n"); 
            
            ScriptBuilder sb = new ScriptBuilder();
            sb.append("DRIVER=\""+selectedDriver+"\"");
            sb.append("DIRECTORY=\""+directory+"\"");
            sb.append("EXECUTABLE=\""+executable+"\"");
            sb.append("APIKEY=\""+apikey+"\"");
            sb.append("WORKSPACE=\""+workspace+"\"");
            sb.append("PULLRESULTS=\""+pullresults+"\"");
            
            // write the cloud.env file
            File file = new File(model.getProject().getBaseDir(),"cloud.env");
            IOUtils.writeLinesToFile(file, sb.getLines());
            file.setExecutable(true);
                
        }else if (selectedDriver.equals("Localhost")){
            
            if (hasCloudEnv==true){
                File file = new File(model.getProject().getBaseDir(),"cloud.env");
                // replace only the DRIVER value (first line) in cloud.env
                cloudEnv.set(0,"DRIVER=\""+selectedDriver+"\"");
                IOUtils.writeLinesToFile(file, cloudEnv);
                file.setExecutable(true);
            }
            
        }
    }

}
